package playing;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import announcement.*;
import cards.*;
import players.*;
import playing.*;
import random.RandomGenerator;
import userinput.UserInput;
import bonuses.*;

class AllTrumpsPlayTest {
	List<Player> players;
	List<Card> hand;
	UserInput input;
	RandomGenerator rand;
	Contract contract = Contract.ALL_TRUMPS;
	int turn = 0;
	Player player1;
	Player player2;
	AllTrumpsPlay playAllTrumps;
	RoundPlay play;
	List<Integer> intChoice;

	void setPlay() {
		players = new ArrayList<>();
		input = Mockito.mock(UserInput.class);
		rand = Mockito.mock(RandomGenerator.class);

		play = new AllTrumpsPlay(players, turn + 1, new Choice(contract, false, false), rand, input);
		playAllTrumps = new AllTrumpsPlay(players, turn + 1, new Choice(contract, false, false), rand, input);
		intChoice = new ArrayList<>(Arrays.asList(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7 }));

	}

	public void setAllPlayers() {
		players.add(new Player("Nelly", PlayerType.PERSON, "Team1"));
		players.add(new Player("Player2", PlayerType.COMPUTER, "Team2"));
		players.add(new Player("Player3", PlayerType.COMPUTER, "Team1"));
		players.add(new Player("Player4", PlayerType.COMPUTER, "Team2"));

	}

	void setPlayer(PlayerType type) {
		player1 = new Player("Nelly", type, "Team1");
		players.add(player1);
	}

	void setHandForBonusTestsAndStrongestCard(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.ACE, Suit.CLUBS));
		hand.add(new Card(Rank.QUEEN, Suit.DIAMONDS));
		hand.add(new Card(Rank.KING, Suit.DIAMONDS));
		hand.add(new Card(Rank.ACE, Suit.DIAMONDS));
		hand.add(new Card(Rank.QUEEN, Suit.HEARTS));
		hand.add(new Card(Rank.ACE, Suit.HEARTS));
		hand.add(new Card(Rank.QUEEN, Suit.SPADES));
		hand.add(new Card(Rank.ACE, Suit.SPADES));

		player.getHand().addAll(hand);

	}

	void setHand(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.ACE, Suit.CLUBS));
		hand.add(new Card(Rank.QUEEN, Suit.HEARTS));
		hand.add(new Card(Rank.KING, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.SPADES));
		hand.add(new Card(Rank.EIGHT, Suit.SPADES));
		hand.add(new Card(Rank.NINE, Suit.SPADES));
		hand.add(new Card(Rank.JACK, Suit.SPADES));
		hand.add(new Card(Rank.KING, Suit.SPADES));

		player.getHand().addAll(hand);

	}

	void setHandForAnnouncementTests(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.ACE, Suit.CLUBS));
		hand.add(new Card(Rank.QUEEN, Suit.HEARTS));
		hand.add(new Card(Rank.KING, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.SPADES));
		hand.add(new Card(Rank.EIGHT, Suit.SPADES));
		hand.add(new Card(Rank.NINE, Suit.SPADES));
		hand.add(new Card(Rank.JACK, Suit.SPADES));
		hand.add(new Card(Rank.KING, Suit.SPADES));

		player.getHand().addAll(hand);

	}

	void setHandFourCards(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.TEN, Suit.CLUBS));
		hand.add(new Card(Rank.KING, Suit.CLUBS));
		hand.add(new Card(Rank.JACK, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.SPADES));

		player.getHand().addAll(hand);

	}

	void setCardsOnTable() {
		play.getCardsOnTable().put(0, new Card(Rank.NINE, Suit.CLUBS));
		play.getCardsOnTable().put(1, new Card(Rank.JACK, Suit.CLUBS));
		play.getCardsOnTable().put(2, new Card(Rank.QUEEN, Suit.HEARTS));
		play.getCardsOnTable().put(3, new Card(Rank.ACE, Suit.DIAMONDS));
		play.getCardNumberAndPerson().put(1, 3);
		play.getCardNumberAndPerson().put(2, 0);
		play.getCardNumberAndPerson().put(3, 1);
		play.getCardNumberAndPerson().put(4, 2);

	}

	@Test
	void When_PlayATurnAndHaveBonuses_Expect_ToReceiveRequestWhetherToAnnounce() {
		setPlay();////////////////////////////////////////
		setPlayer(PlayerType.PERSON);
		when(input.getInput()).thenReturn("yes");
		when(input.chooseCardFromList(player1.getHand(), 1, 8));
	}

	@Test
	void When_CardIsNotNull_Expect_ToReturnTheCorrespondingPoints() {
		setPlay();
		Card card1 = new Card(Rank.JACK, Suit.HEARTS);
		Card card2 = new Card(Rank.SEVEN, Suit.HEARTS);
		assertEquals(20, play.getCardPoints(card1));
		assertEquals(0, play.getCardPoints(card2));

	}

	@Test
	void When_PlayerContainsStrongerCard_ExpectToReturnTheFirstOccurence() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);
		Card card2 = new Card(Rank.SEVEN, Suit.HEARTS);
		assertEquals(4, playAllTrumps.containsStrongerCardOfSuit(card2, player1));
	}

	@Test
	void When_CardPlayerDoesNotContainStrongerCard_ExpectToReturnMinusOne() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);
		Card card = new Card(Rank.JACK, Suit.HEARTS);
		assertEquals(-1, playAllTrumps.containsStrongerCardOfSuit(card, player1));
	}

	@Test
	void When_RemoveBeloteCards_Expect_PlayerHandNotToContainBelote() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);
		List<Card> belote = new ArrayList<>();
		belote.add(hand.get(1));
		belote.add(hand.get(2));
		player1.setBelote(belote);
		playAllTrumps.removeBeloteCards(player1, player1.getHand().get(1));
		assertFalse(player1.getBelote().stream().anyMatch(x -> x.equals(hand.get(1))));
		assertFalse(player1.getBelote().stream().anyMatch(x -> x.equals(hand.get(2))));
	}

	@Test
	void When_BeloteCountIsOne_Expect_ValidPoints() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		player1.setBeloteCount(1);
		playAllTrumps.addBelotePoints();
		assertEquals(20, player1.getPoints());
	}

	@Test
	void When_BeloteCountIsTwo_Expect_ValidPoints() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		player1.setBeloteCount(2);
		playAllTrumps.addBelotePoints();
		assertEquals(40, player1.getPoints());
	}

	@Test
	void When_GivenNumberIsExistingBonusTypeNumber_Expect_ToReturnValidBonusType() {
		setPlay();
		assertEquals(BonusType.TIERCE, playAllTrumps.determineTypeSequences(3));
		assertEquals(BonusType.QUARTE, playAllTrumps.determineTypeSequences(4));
		assertEquals(BonusType.QUINT, playAllTrumps.determineTypeSequences(5));
		assertEquals(BonusType.QUINT, playAllTrumps.determineTypeSequences(6));
		assertEquals(BonusType.QUINT, playAllTrumps.determineTypeSequences(7));

	}

	@Test
	void When_GivenNumberIsNotExistingBonusTypeNumber_Expect_ToReturnValidBonusType() {
		setPlay();
		assertEquals(BonusType.QUINT, playAllTrumps.determineTypeSequences(10));

	}

	@Test
	void When_PlayerHandContainsOnePairOfBelote_Expect_ResultToMatch() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);

		List<Card> cards = new ArrayList<>();
		cards.add(hand.get(1));
		cards.add(hand.get(2));
		assertEquals(playAllTrumps.getBelote(hand, contract), cards);
		contract = Contract.CLUBS;

		assertEquals(playAllTrumps.getBelote(hand, contract), new ArrayList<>());

	}

	@Test
	void When_HandContainsTierce_Expect_MatchingLists() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);
		List<Bonus> expected = new ArrayList<>();
		Set<Card> bonus = new HashSet<>();
		bonus.add(hand.get(1));
		bonus.add(hand.get(2));
		bonus.add(hand.get(3));
		expected.add(new Bonus(bonus, BonusType.TIERCE));

		assertEquals(expected, playAllTrumps.consecutiveSuits(hand));

	}

	@Test
	void When_HandContainsPairOfTierces_Expect_MatchingResult() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		hand = new ArrayList<>();
		hand.add(new Card(Rank.EIGHT, Suit.CLUBS));
		hand.add(new Card(Rank.NINE, Suit.CLUBS));
		hand.add(new Card(Rank.TEN, Suit.CLUBS));
		hand.add(new Card(Rank.SEVEN, Suit.DIAMONDS));
		hand.add(new Card(Rank.QUEEN, Suit.HEARTS));
		hand.add(new Card(Rank.KING, Suit.HEARTS));
		hand.add(new Card(Rank.ACE, Suit.HEARTS));
		hand.add(new Card(Rank.ACE, Suit.SPADES));
		player1.getHand().addAll(hand);

		List<Bonus> expected = new ArrayList<>();
		Set<Card> bonus1 = new HashSet<>();
		bonus1.add(hand.get(0));
		bonus1.add(hand.get(1));
		bonus1.add(hand.get(2));
		Set<Card> bonus2 = new HashSet<>();
		bonus2.add(hand.get(4));
		bonus2.add(hand.get(5));
		bonus2.add(hand.get(6));

		expected.add(new Bonus(bonus1, BonusType.TIERCE));
		expected.add(new Bonus(bonus2, BonusType.TIERCE));
		assertEquals(expected, playAllTrumps.consecutiveSuits(hand));

	}

	@Test
	void When_HandContainsSquareOfAces_Expect_MatchingLists() {
		setPlay();
		setPlayer(PlayerType.PERSON);
		setHandForBonusTestsAndStrongestCard(player1);

		List<Bonus> expected = new ArrayList<>();
		Set<Card> bonus = new HashSet<>();
		bonus.add(hand.get(0));
		bonus.add(hand.get(3));
		bonus.add(hand.get(5));
		bonus.add(hand.get(7));
		expected.add(new Bonus(bonus, BonusType.SQUARE_ACES));

		assertEquals(expected, playAllTrumps.getSquares(hand));

	}

	@Test
	void When_PlayerAnswerIsZero_Expect_BonusesToMatch() {
		setPlay();
		setPlayer(PlayerType.COMPUTER);
		setHandForAnnouncementTests(player1);
		List<Bonus> bonuses = new ArrayList<>();
        Set<Card> set = new HashSet<>();

		set.add(hand.get(3));
		set.add(hand.get(4));
		set.add(hand.get(5));
		bonuses.add(new Bonus(set, BonusType.TIERCE));

		when(rand.generateNumber(0, 2)).thenReturn(0);
		assertNotEquals(bonuses,playAllTrumps.announceBonuses(player1));
		
		
	}


	@Test
	void When_PlayerAnswerIsOne_Expect_BonusesToMatch() {
		setPlay();
		setPlayer(PlayerType.COMPUTER);
		setHandForAnnouncementTests(player1);
		List<Bonus> bonuses = new ArrayList<>();
        Set<Card> set = new HashSet<>();

		set.add(hand.get(3));
		set.add(hand.get(4));
		set.add(hand.get(5));
		bonuses.add(new Bonus(set, BonusType.TIERCE));

		when(rand.generateNumber(0, 2)).thenReturn(1);
		assertEquals(bonuses,playAllTrumps.announceBonuses(player1));
		
		
	}

//	@Test
//	void testAnnounceBelote() {
//		when(rand.generateNumber(0, 2)).thenReturn(1);
//		assertEquals("yes", playAllTrumps.computerAnnouncement());
//		playAllTrumps.announceBelote(player2, player2.getHand().get(1));
//		assertEquals(1, player2.getBeloteCount());
//		assertTrue(player2.getBelote().stream().allMatch(x -> x.equals(hand.get(1))));
//		when(rand.generateNumber(0, 2)).thenReturn(0);
//		player2.setBeloteCount(0);
//		playAllTrumps.announceBelote(player2, player2.getHand().get(1));
//		assertEquals(0, player2.getBeloteCount());
//	}
//
//	@Test
//	void testComputerAnnouncement() {
//		when(rand.generateNumber(0, 2)).thenReturn(1);
//		assertEquals("yes", playAllTrumps.computerAnnouncement());
//		when(rand.generateNumber(0, 2)).thenReturn(0);
//		assertEquals("no", playAllTrumps.computerAnnouncement());
//
//	}
//
//	@Test
//	void testPersonAnnouncement() {
//		String answer = "yes";
//		InputStream in = new ByteArrayInputStream(answer.getBytes());
//		System.setIn(in);
//		assertEquals(answer, playAllTrumps.personAnnouncement(player1));
//	}

}
