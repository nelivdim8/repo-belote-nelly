package playing;

import playing.*;
import random.RandomGenerator;
import userinput.UserInput;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import announcement.Choice;
import announcement.Contract;
import cards.Card;
import cards.Rank;
import cards.Suit;
import players.Player;
import players.PlayerType;

class ColorPlayTest {
	ColorPlay play;
	List<Player> players;
	Player player1;
	Player player2;
	UserInput input;
	List<Card> hand;
	RandomGenerator rand;
	List<Integer> intChoice;

	public void setAllPlayers() {
		players.add(new Player("Nelly", PlayerType.PERSON, "Team1"));
		players.add(new Player("Player2", PlayerType.COMPUTER, "Team2"));
		players.add(new Player("Player3", PlayerType.COMPUTER, "Team1"));
		players.add(new Player("Player4", PlayerType.COMPUTER, "Team2"));

	}

	void setPlayer(PlayerType type) {
		player1 = new Player("Nelly", type, "Team1");
		players.add(player1);
	}

	void setHand(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.ACE, Suit.CLUBS));
		hand.add(new Card(Rank.QUEEN, Suit.HEARTS));
		hand.add(new Card(Rank.JACK, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.SPADES));
		hand.add(new Card(Rank.EIGHT, Suit.SPADES));
		hand.add(new Card(Rank.NINE, Suit.SPADES));
		hand.add(new Card(Rank.JACK, Suit.SPADES));
		hand.add(new Card(Rank.KING, Suit.SPADES));
		player.getHand().addAll(hand);

	}

	void setHandFourCards(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.TEN, Suit.CLUBS));
		hand.add(new Card(Rank.KING, Suit.CLUBS));
		hand.add(new Card(Rank.JACK, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.SPADES));

		player.getHand().addAll(hand);

	}
	void setHandWithCardsToRespondAndNoTrumpSuit(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.TEN, Suit.DIAMONDS));
		hand.add(new Card(Rank.KING, Suit.DIAMONDS));
		hand.add(new Card(Rank.JACK, Suit.HEARTS));
		hand.add(new Card(Rank.SEVEN, Suit.HEARTS));

		player.getHand().addAll(hand);

	}
	void setHandContainingTrumpSuit(Player player) {
		hand = new ArrayList<>();
		hand.add(new Card(Rank.KING, Suit.DIAMONDS));
		hand.add(new Card(Rank.EIGHT, Suit.SPADES));
		hand.add(new Card(Rank.JACK, Suit.SPADES));
		hand.add(new Card(Rank.KING, Suit.SPADES));
		
	
		

		player.getHand().addAll(hand);

	}

	void setCardsOnTableNoTrumpSuits() {
		play.getCardsOnTable().put(0, new Card(Rank.NINE, Suit.CLUBS));
		play.getCardsOnTable().put(1, new Card(Rank.ACE, Suit.CLUBS));
		play.getCardNumberAndPerson().put(1, 0);
		play.getCardNumberAndPerson().put(2, 1);
		

	}
	void setCardsOnTableHasTrumped() {
		play.getCardsOnTable().put(0, new Card(Rank.NINE, Suit.CLUBS));
		play.getCardsOnTable().put(1, new Card(Rank.TEN, Suit.SPADES));
		play.getCardNumberAndPerson().put(1, 0);
		play.getCardNumberAndPerson().put(2, 1);
		

	}
	void setCardsOnTableTrumpSuits() {
		play.getCardsOnTable().put(0, new Card(Rank.NINE,Suit.SPADES));
		play.getCardsOnTable().put(1, new Card(Rank.SEVEN, Suit.CLUBS));
		play.getCardNumberAndPerson().put(1, 0);
		play.getCardNumberAndPerson().put(2, 1);
		

	}

	void setPlay(Contract contract) {
		players = new ArrayList<>();
		input = Mockito.mock(UserInput.class);
		rand = Mockito.mock(RandomGenerator.class);
		play = new ColorPlay(players, 1, new Choice(contract, false, false), rand, input);
		intChoice = new ArrayList<>(Arrays.asList(new Integer[] { 0, 1, 2, 3, 4, 5, 6, 7 }));

	}

	@Test
	void When_ArgumentIsIndexOutOfBoundOfList_Expect_IndexOutOfBoundsException() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		assertThrows(IndexOutOfBoundsException.class, () -> play.getCardPoints(hand.get(20)));
	}

	@Test

	void When_ArgumentIsIndexOfBoundOfList_Expect_ToReturnPoints() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		assertEquals(20, play.getCardPoints(hand.get(6)));
	}

	@Test
	void When_StrongestCardIsThrownByPlayerWithIndexOne_Expect_ToReturnOne() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		setCardsOnTableNoTrumpSuits();
		assertEquals(1, play.determinePlayerWithStrongestCard());
		play.getCardsOnTable().put(2, new Card(Rank.NINE, Suit.SPADES));
		play.getCardNumberAndPerson().put(3, 2);
		play.setHasTrumped(true);
		assertEquals(2, play.determinePlayerWithStrongestCard());

	}

	@Test
	void When_StrongestCardIsThrownByPlayerWithIndexTwo_Expect_ToReturnTwo() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);
		setCardsOnTableNoTrumpSuits();
		play.getCardsOnTable().put(2, new Card(Rank.NINE, Suit.SPADES));
		play.getCardNumberAndPerson().put(3, 2);
		play.setHasTrumped(true);
		assertEquals(2, play.determinePlayerWithStrongestCard());

	}

	@Test
	void When_PersonHasCardOfNoTrumpSuit_Expect_ToReturnOneCardOfThatSuit() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandFourCards(player1);
		setCardsOnTableNoTrumpSuits();
		when(input.chooseCardFromList(intChoice.subList(0, 2), 1, 1)).thenReturn(1);
		int choice = play.personChoice(player1, 3);
		assertEquals(1, choice);

	}
	@Test
	void When_PersonHasCardOfTrumpSuit_Expect_ToReturnOneCardOfThatSuit() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandContainingTrumpSuit(player1);
		setCardsOnTableTrumpSuits();
		when(input.chooseCardFromList(intChoice.subList(2, 3), 3, 2)).thenReturn(2);
		int choice = play.personChoice(player1, 3);
		assertEquals(2, choice);

	}
	@Test
	void When_PersonCanNotRespondButHasCardOfTrumpSuit_Expect_ToReturnOneCardOfThatSuit() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandContainingTrumpSuit(player1);
		setCardsOnTableNoTrumpSuits();
		when(input.chooseCardFromList(intChoice.subList(1, 4), 2, 3)).thenReturn(2);
		int choice = play.personChoice(player1, 3);
		assertEquals(2, choice);

	}
	@Test
	void When_HasTrumpedAndPersonHasStrongerTrumpCards_Expect_ToReturnOneThatCards() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandContainingTrumpSuit(player1);
		setCardsOnTableHasTrumped();
		play.setHasTrumped(true);
		when(input.chooseCardFromList(intChoice.subList(2, 3), 3, 2)).thenReturn(2);
		int choice = play.personChoice(player1, 3);
		assertEquals(2, choice);

	}
	@Test
	void When_HasTrumpedAndPersonDoesNotHaveStrongerTrumpCards_Expect_ToReturnOneOfAllCards() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandContainingTrumpSuit(player1);
		player1.getHand().remove(2);
		setCardsOnTableHasTrumped();
		play.setHasTrumped(true);
		when(input.chooseCardFromList(player1.getHand(), 1, 2)).thenReturn(2);
		int choice = play.personChoice(player1, 3);
		assertEquals(2, choice);

	}
	
	@Test
	void When_PersonCanNotRespondAndDoesNotHaveCardOfTrumpSuit_Expect_ToReturnOneCardOfThatSuit() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandWithCardsToRespondAndNoTrumpSuit(player1);
		setCardsOnTableNoTrumpSuits();
		when(input.chooseCardFromList(hand, 1, 3)).thenReturn(3);
		int choice = play.personChoice(player1, 3);
		assertEquals(3, choice);

	}
	
	
	@Test
	void When_CardThrownIsFirst_Expect_ToReturnTheSameValue() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);
		when(input.chooseCardFromList(hand, 1, 7)).thenReturn(6);
		assertEquals(6,play.personChoice(player1, 1));

	}
	@Test
	void When_PersonDoesNotHaveCardOfSuit_Expect_ToReturnCurrentCardFromAll() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);
		when(input.chooseCardFromList(hand, 1, 7)).thenReturn(6);
		assertEquals(6,play.personChoice(player1, 1));

	}

	@Test
	void When_PersonInputIsOfRangeValues_Expect_NotToReturnNegativeNumber() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHandFourCards(player1);

		setCardsOnTableNoTrumpSuits();
		when(input.chooseCardFromList(intChoice.subList(0, 2), 1, 1)).thenReturn(1);

		assertNotEquals(-1, play.personChoice(player1, 3));

	}

	@Test
	void When_PersonInputIsOfRangeValues_Expect_NotToReturnValueGreaterThanMaxOfRange() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		setCardsOnTableNoTrumpSuits();
		when(input.chooseCardFromList(intChoice.subList(0, 5), 1, 4)).thenReturn(2);

		assertNotEquals(4, play.personChoice(player1, 3));

	}

	@Test
	void When_ComputerGeneratedValueIfOfRangeValues_Expect_ToReturnTheSameValue() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHandFourCards(player1);
		setCardsOnTableNoTrumpSuits();

		when(rand.generateNumber(0, 1)).thenReturn(0);
		assertEquals(0, play.computerChoice(player1, 3));

	}
	@Test
	void When_ComputerDoesNotHaveCardOfSuit_Expect_ToReturnCurrentCardFromAll() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHandWithCardsToRespondAndNoTrumpSuit(player1);
		setCardsOnTableNoTrumpSuits();
		when(rand.generateNumber(0, 4)).thenReturn(3);
		assertEquals(3,play.computerChoice(player1, 3));

	}

	@Test
	void When_ComputerPlayerShoudChooseFirstCardToThrow_Expect_ToReturnRandomFromAll() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHand(player1);
	    when(rand.generateNumber(0, 8)).thenReturn(5);
		assertEquals(5, play.computerChoice(player1, 1));

	}
	@Test
	void When_HasTrumpedAndComputerDoesNotHaveStrongerTrumpCards_Expect_ToReturnRandomFromAll() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHandContainingTrumpSuit(player1);
		player1.getHand().remove(2);
		setCardsOnTableHasTrumped();
		play.setHasTrumped(true);
		when(rand.generateNumber(0, 3)).thenReturn(2);
		int choice = play.computerChoice(player1, 3);
		assertEquals(2, choice);

	}
	@Test
	void When_HasTrumpedAndComputerHasStrongerTrumpCards_Expect_ToReturnOneThatCards() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHandContainingTrumpSuit(player1);
		setCardsOnTableHasTrumped();
		play.setHasTrumped(true);
		when(rand.generateNumber(2, 1)).thenReturn(2);
		int choice = play.computerChoice(player1, 3);
		assertEquals(2, choice);

	}


	@Test
	void When_ComputerGeneratedValueIfOfRangeValues_Expect_NotToReturnNegativeValue() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		setCardsOnTableNoTrumpSuits();
		player1.getHand().remove(0);
		player1.getHand().remove(0);
		player1.getHand().remove(0);
		when(rand.generateNumber(0, 5)).thenReturn(1);
		assertNotEquals(-1, play.computerChoice(player1, 3));

	}
	@Test
	void When_ComputerHasCardOfTrumpSuit_Expect_ToReturnOneCardOfThatSuit() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.COMPUTER);
		setHandContainingTrumpSuit(player1);
		setCardsOnTableTrumpSuits();
		when(rand.generateNumber(2, 1)).thenReturn(2);
		int choice = play.computerChoice(player1, 3);
		assertEquals(2,choice);
		
		
	}
	


	@Test
	void When_PlayerHasTrumpSuit_Expect_ToReturnFalse() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		assertFalse(play.hasTrumpSuit(hand.get(2)));
	}

	@Test
	void When_PlayerHasTrumpSuit_Expect_ToReturnTrue() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		assertTrue(play.hasTrumpSuit(hand.get(6)));

	}

	@Test
	void When_SuitIsTrump_Expect_ToReturnTrue() {
		setPlay(Contract.SPADES);

		assertTrue(play.isTrumpSuit(Suit.SPADES));
	}

	@Test
	void When_SuitIsNotTrump_Expect_ToReturnFalse() {
		setPlay(Contract.SPADES);

		assertFalse(play.isTrumpSuit(Suit.CLUBS));

	}

	@Test
	void WhenExpectedIsNotTrump_Expect_ResultsNotToMatch() {
		setPlay(Contract.SPADES);

		assertNotEquals(Suit.CLUBS, play.getTrumpSuit());

	}

	@Test
	void WhenExpectedIsTrump_Expect_ResultsToMatch() {
		setPlay(Contract.SPADES);

		assertEquals(Suit.SPADES, play.getTrumpSuit());
	}

	@Test
	void When_PlayerContainsTrumpSuit_Expect_ToReturnTrue() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);

		assertTrue(play.containsTrumpSuit(player1));

	}

	@Test
	void When_PlayerDoesNotContainTrumpSuit_Expect_ToReturnFalse() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);
		setHand(player1);
		player1.getHand().removeIf(x -> x.getSuit().equals(Suit.SPADES));
		assertFalse(play.containsTrumpSuit(player1));

	}

	@Test
	void When_PlayerDoesNotContainAnyCards_Expect_ToReturnFalse() {
		setPlay(Contract.SPADES);
		setPlayer(PlayerType.PERSON);

		assertFalse(play.containsTrumpSuit(player1));

	}

}
