package random;

import java.util.Random;

public class RandomGenerator {
	private Random rand;

	public RandomGenerator(long seed) {
		this.rand = new Random(seed);
	}

	public RandomGenerator() {
		this.rand = new Random();
	}

	public int generateNumber(int from, int length) {
		int number = from + rand.nextInt(length);

		return number;
	}

}
