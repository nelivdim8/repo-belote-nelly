package announcement;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public enum Contract {
CLUBS(1, "Clubs"), DIAMONDS(2, "Diamonds"), HEARTS(3,"Hearts"),SPADES(4, "Spades"),NO_TRUMPS(5, "No trumps"),
ALL_TRUMPS(6, "All trumps"),PASS(7, "Pass"),CONTRA(8,"Contra"),RECONTRA(9,"Recontra");
	private final int value;
	private String contractName;

	private static Map<Integer, Contract> getContractByNumber = new HashMap<>();

	static {
		for (Contract contract : Contract.values()) {
			getContractByNumber.put(contract.value, contract);
		}
	}

	private Contract(int value, String contractName) {
		this.value = value;
		this.contractName = contractName;
	}

	public int getValue() {
		return value;
	}

	public String getContractName() {
		return contractName;
	}

	public void setContractName(String contractName) {
		this.contractName = contractName;
	}

	public static Map<Integer, Contract> getGetContractByNumber() {
		return getContractByNumber;
	}

	public static void setGetContractByNumber(Map<Integer, Contract> getContractByNumber) {
		Contract.getContractByNumber = getContractByNumber;
	}

	public static List<Integer> getNums() {
		List<Integer> list = new ArrayList<>(getContractByNumber.keySet());
		list.sort(Comparator.naturalOrder());
		return list;
	}

	public static void showOptions(int from, int to) {
		Contract[] values = Contract.values();
		for (int i = from-1; i < to; i++) {
			System.out.println(values[i].value + ". : " + values[i].contractName);

		}
	}
	public static void showOptions(int from, int to,int elseOption) {
	showOptions(from, to);
	System.out.println(elseOption+". : "+getContractByNumber.get(elseOption).contractName);
	}

}
