package announcement;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Random;
import java.util.stream.Collectors;

import dealing.Dealing;
import players.Player;
import userinput.UserInput;
import view.View;

public class Announcement {
    private Random rand = new Random();
	private Map<Integer, Choice> announced = new HashMap<>();
	private int pass;
	

	private UserInput input;

	public Announcement(UserInput input) {
		this.input = input;
	}

	public Choice announce(List<Player> players,int i) {
		boolean gameProposed = false;
		while (pass < 3 || (pass == 4 && !gameProposed)) {
			Choice choice = playerAnnounce(i);
			
			System.out.print(players.get(i).getName() + ": ");
			System.out.println(choice.toString());
			View.slowDown();

			
			if (choice.getNumber() == 7) {
				pass++;
			} else {
				pass = 0;
				gameProposed = true;
			}
			i++;
			if (i > 3) {
				i = 0;
			}
		}
		getMax().getValue().setAnnouncedTeam(players.get(getMax().getKey()).getTeam());
		players.get(getMax().getKey()).setHasAnnounced(true);
		return getMax().getValue();

	}

	private Choice playerAnnounce(int i) {
		if (i == 0) {
			return personAnnounce(i);
		}
		return computerAnnounce(i);

	}

	private Choice personAnnounce(int i) {
		int choice = 0;
		Choice max = null;
		if (!containsOneOfGiven()) {
			choice = input.chooseTypeOfGame(1, 7);
		} else {
			int person = getMax().getKey();
			max = new Choice(announced.get(person));
			if (areOneTeam(person, i)) {
				max.setNumber(max.getNumber() + 1);
				choice = input.chooseTypeOfGame(max.getNumber(), 7);

			} else {

				if (max.isContra()) {
					choice = input.chooseBetween(9, 7, max);
				} else if (max.isRecontra()) {
					max.setNumber(max.getNumber() + 1);
					choice = input.chooseTypeOfGame(max.getNumber(), 7);
				} else {
					choice = input.chooseBetween(8, 7, max);
				}
			}
		}

		return addChoice(choice, i, max);

	}

	private Choice computerAnnounce(int i) {
		int choice = 0;
		Choice max = null;
		if (!containsOneOfGiven()) {
			choice = 1 + rand.nextInt(7);
		} else {
			int person = getMax().getKey();
			max = new Choice(announced.get(person));
			if (areOneTeam(person, i)) {
				max.setNumber(max.getNumber() + 1);
				choice = max.getNumber() + rand.nextInt(8 - max.getNumber());

			} else {

				if (max.isContra()) {
					choice =randBetween(9, 7, max);
				} else if (max.isRecontra()) {
					max.setNumber(max.getNumber() + 1);
					choice = max.getNumber() + rand.nextInt(8 - max.getNumber());
				} else {
					choice = randBetween(8, 7, max);
				}
			}
		}

		return addChoice(choice, i, max);

	}

	private int randBetween(int i, int number, Choice max) {
		int random = rand.nextInt(2);

		if (random == 1) {
			return i;
		} else {
			max.setNumber(max.getNumber() + 1);
			return max.getNumber() + rand.nextInt(number + 1 - max.getNumber());
		}
	}

	public boolean containsOneOfGiven() {
		return announced.entrySet().stream()
				.anyMatch(x -> x.getValue().getNumber() >= 1 && x.getValue().getNumber() <= 6);
	}

	public boolean areOneTeam(int i, int j) {

		if ((i % 2 == 0 && j % 2 == 0) || (i % 2 != 0 && j % 2 != 0)) {
			return true;
		}
		return false;
	}

	public Choice addChoice(int choice, int person, Choice max) {
		if (choice == 8) {
			announced.put(person, new Choice(max.getNumber(), true, false));

		} else if (choice == 9) {
			announced.put(person, new Choice(max.getNumber(), false, true));
		} else {
			announced.put(person, new Choice(choice));
		}
		return announced.get(person);

	}

	public Map<Integer, Choice> getAnnounced() {
		return announced;
	}

	public void setAnnounced(Map<Integer, Choice> announced) {
		this.announced = announced;
	}

	public Map.Entry<Integer, Choice> getMax() {
		int max = announced.entrySet().stream().filter(x -> x.getValue().getNumber() != 7)
				.map(x -> x.getValue().getNumber()).reduce((a, b) -> Integer.max(a, b)).get();

		List<Map.Entry<Integer, Choice>> elements = announced.entrySet().stream()
				.filter(x -> x.getValue().getNumber() == max).collect(Collectors.toList());

		if (elements.size() > 1) {
			return findMaxAnnounce(elements);
		}
		return elements.get(0);
	}

	public Map.Entry<Integer, Choice> findMaxAnnounce(List<Entry<Integer, Choice>> elements) {
		for (int i = 0; i < elements.size(); i++) {
			if (elements.get(i).getValue().isRecontra()) {
				return elements.get(i);

			}
		}

		if (elements.get(0).getValue().isContra()) {
			return elements.get(0);

		}
		return elements.get(1);

	}

}