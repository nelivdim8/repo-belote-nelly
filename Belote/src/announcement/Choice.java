package announcement;

public class Choice {
	 private Contract contract;
	 private boolean isContra;
	 private boolean isRecontra;
	 private String announcedTeam;
	 

	public String getAnnouncedTeam() {
		return announcedTeam;
	}


	public void setAnnouncedTeam(String announcedTeam) {
		this.announcedTeam = announcedTeam;
	}


	public Choice(int number, boolean isContra, boolean isRecontra) {
		super();
		this.contract = Contract.getGetContractByNumber().get(number);
		this.isContra = isContra;
		this.isRecontra = isRecontra;
	}
	public Choice(Contract contract, boolean isContra, boolean isRecontra) {
		super();
		this.contract = contract;
		this.isContra = isContra;
		this.isRecontra = isRecontra;
	}
	
	

	public Choice(int number) {
		super();
		this.contract = Contract.getGetContractByNumber().get(number);
		this.isContra =false;
		this.isRecontra = false;
	}

	public Choice(Choice c) {
		super();
		this.contract =c.contract;
		this.isContra =c.isContra;
		this.isRecontra = c.isRecontra;
	}


	public Contract getContract() {
		return contract;
	}
	public int getNumber() {
		return contract.getValue();
	}


	public void setNumber(int number) {
	if(this.contract.getValue()<=6&&contract.getValue()<=7) {
		this.contract = Contract.getGetContractByNumber().get(number);
	}
	}

	public boolean isContra() {
		return isContra;
	}

	public void setContra(boolean isContra) {
		this.isContra = isContra;
	}

	public boolean isRecontra() {
		return isRecontra;
	}

	public void setRecontra(boolean isRecontra) {
		this.isRecontra = isRecontra;
	}

	@Override
	public String toString() {
		return contract.getContractName() + (isContra ? " Contra": "") +(isRecontra? " Recontra": "");
	}
	
	 


}
