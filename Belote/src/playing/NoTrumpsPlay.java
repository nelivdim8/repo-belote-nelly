package playing;

import cards.*;
import players.Player;
import players.PlayerType;
import random.RandomGenerator;
import userinput.UserInput;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import java.util.function.Predicate;

import announcement.Choice;

public class NoTrumpsPlay extends RoundPlay {
	private static final int ALL_HANDS_TAKEN=350;
	private static final int COEFFICIENT=2;
	private static final double ROUNDING =0.5;

	Comparator<Card> byCardPoints = (c1, c2) -> Integer.compare(getCardPoints(c1), getCardPoints(c2));

	Comparator<Card> byRankValue = (c1, c2) -> Integer.compare(c1.getRankValue(), c2.getRankValue());

	Comparator<Card> compareTwoCards = byCardPoints.thenComparing(byRankValue);


	public NoTrumpsPlay(List<Player> players, int turn, Choice choice, RandomGenerator rand, UserInput input) {
		super(players, turn, choice, rand, input);
	
	}
	public Comparator<Card> getCompareTwoCards() {
		return compareTwoCards;
	}
	public int getAllHandsTaken() {
		return ALL_HANDS_TAKEN;
	}
	public double getPartFromWhereToRoundUp() {
		return ROUNDING;
	}
	public int getCoefficient() {
		return COEFFICIENT;
	}

	@Override
	public int getCardPoints(Card c) {
		return c.getRank().getNoTrumpsPoints();
	}

	@Override
	public int determinePlayerWithStrongestCard() {
		int personIndex = super.getCardNumberAndPerson().get(1);
		Player first = super.getPlayers().get(personIndex);
		Card firstCard = super.getCardsOnTable().get(personIndex);
		Suit suit = firstCard.getSuit();
        return super.personAndCardsOnTableGetPerson(getStrongest(suit));

	}

	public int declareHandTaker() {
		int playerIndex = determinePlayerWithStrongestCard();
		Player winner = super.getPlayers().get(playerIndex);
		winner.addPoints(sumAllPoints());
		System.out.println(winner.getName() + " takes the cards!");
		super.getCardsOnTable().clear();

		return playerIndex;

	}

	public int sumAllPoints() {
		int sum = 0;
		Collection<Card> cards = super.getCardsOnTable().values();
		List<Card> listOfCards = new ArrayList<>(cards);
		for (int i = 0; i < listOfCards.size(); i++) {
			sum += getCardPoints(listOfCards.get(i));

		}
		return sum;
	}

	@Override
	public int scoring(String team) {
		String winners = super.getRoundWinners();
		if (winners.equals(team)) {
			if (!super.areAllHandsTaken()) {
				if (super.getChoice().isContra()) {
					return 2 * scoring(team, getPartFromWhereToRoundUp(), getCoefficient());
				}
				if (super.getChoice().isRecontra()) {
					return 4 * scoring(team, getPartFromWhereToRoundUp(), getCoefficient());
				}
				return scoring(team, getPartFromWhereToRoundUp(), getCoefficient());
			}
		}
		if (winners.equals("")) {
			if ((super.getChoice().isContra() || super.getChoice().isRecontra())) {
				addHangingPoints(scoring(team, getPartFromWhereToRoundUp(), getCoefficient()));
				return 0;

			} else {
				if (team.equals(super.getChoice().getAnnouncedTeam())) {
					addHangingPoints(scoring(team, getPartFromWhereToRoundUp(), getCoefficient()));
					return 0;
				}
				return  scoring(team, getPartFromWhereToRoundUp(), getCoefficient());
			}
		}
		return scoring(team, getPartFromWhereToRoundUp(), getCoefficient());
	}

	public int scoring(String team, double partFromWhereToRoundUp, int coefficient) {

		double points = (team.equals("Team1")) ? super.getRoundPointsTeam1()
				: (team.equals("Team2")) ? super.getRoundPointsTeam2() : 0.0;
		if (points != 0.0) {
			points = (points * coefficient) / 10;
			String stringPoints = String.valueOf(points);
			String value = stringPoints.substring(stringPoints.indexOf(".") + 1, stringPoints.indexOf(".") + 2);
			int whole = (int) points;
			int fractionalPart = Integer.parseInt(value);
			points = (fractionalPart >= partFromWhereToRoundUp * 10) ? (whole + 1) : (whole);
			int result = (int) points;
			return result;
		}
		return 0;
	}

	public Card getStrongest(Suit suit) {
		Card max = super.getCardsOnTable().entrySet().stream().filter(x -> x.getValue().getSuit().equals(suit))
				.map(x -> x.getValue()).max(compareTwoCards).get();
		return max;
	}

	@Override
	public Card playATurn(Player player, int cardThrown) {
		int cardIndex = chooseCard(player, cardThrown);
		Card removed = player.getHand().remove(cardIndex);
		return putCardOnTable(player, cardThrown, removed);
	}

	@Override
	public int chooseCard(Player player, int cardThrown) {
		int cardIndex = 0;

		if (player.getType() == PlayerType.PERSON) {
			return cardIndex = personChoice(player, cardThrown);

		} else {
			return cardIndex = computerChoice(player, cardThrown);
		}

	}

	@Override
	public int personChoice(Player player, int cardThrown) {
		int cardIndex = 0;
		if (cardThrown == 1) {
			cardIndex = super.getUserInput().chooseCardFromList(player.getHand(), 1, player.getHand().size() - 1);
		} else {
			Suit suit = super.getFirstThrownCard().getSuit();
			if (super.containsSuit(suit, player)) {
				cardIndex = personChoiceOfSameSuit(player, suit);
			} else {
				cardIndex = super.getUserInput().chooseCardFromList(player.getHand(), 1, player.getHand().size() - 1);
			}

		}
		return cardIndex;

	}

	@Override
	public int computerChoice(Player player, int cardThrown) {
		int cardIndex = 0;
		if (cardThrown == 1) {
			cardIndex = super.getRand().generateNumber(0, player.getHand().size());
		} else {
			Card first = super.getFirstThrownCard();
			Suit suit = first.getSuit();

			if (super.containsSuit(suit, player)) {
				cardIndex = computerChoiceOfSameSuit(player, suit);

			} else {
				cardIndex = super.getRand().generateNumber(0, player.getHand().size());
			}

		}
		return cardIndex;

	}

	public int personChoiceOfSameSuit(Player player, Suit suit) {
		List<Integer> choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));

		return super.getUserInput().chooseCardFromList(choice, choice.get(0) + 1, choice.get(choice.size() - 1));

	}

	public int computerChoiceOfSameSuit(Player player, Suit suit) {
		List<Integer> choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));

		return super.getRand().generateNumber(choice.get(0), choice.size());

	}

	@Override
	public Card putCardOnTable(Player player, int cardThrown, Card removed) {

		super.getCardsOnTable().put(super.getTurn(), removed);
		super.getCardNumberAndPerson().put(cardThrown, super.getTurn());

		return removed;
	}

	@Override
	public boolean addFinalRoundPoints() {
		addTeamPoints();
		checkTeamForNoHands();
		if (checkWhoAreWinners().equals("not announced")) {
			movePointsFromOneTeamToOther();
			super.redistributeTeamPoints();
			return true;
		}
		return false;
	}

	@Override
	public String checkWhoAreWinners() {
		String announcedTeam = super.getChoice().getAnnouncedTeam();
		String winners = getRoundWinners();
		if (announcedTeam.equals(winners)) {

			return "announced";
		} else if (winners.equals("")) {
			System.out.println("Equal result!");
			return "";
		}

		System.out.println(announcedTeam + " are in!");

		return "not announced";
	}

	@Override
	public void movePointsFromOneTeamToOther() {
		String announcedTeam = super.getChoice().getAnnouncedTeam();
		String winners = getRoundWinners();

		Player winner = super.getPlayers().stream().filter(x -> x.getTeam().equals(winners)).findFirst().get();
		for (int i = 0; i < super.getPlayers().size(); i++) {
			if (super.getPlayers().get(i).getTeam().equals(announcedTeam)) {

				winner.addPoints(super.getPlayers().get(i).getPoints());
				super.getPlayers().get(i).setPoints(0);
			}
		}
	}

	public boolean checkTeamForNoHands() {
		if (getRoundPointsTeam1() == 0) {
			setRoundPointsTeam2(getAllHandsTaken());
			setAreAllHandsTaken(true);
			System.out.println("Team1 has no hand taken");
			return true;
		}
		if (getRoundPointsTeam2() == 0) {
			setRoundPointsTeam1(getAllHandsTaken());
			setAreAllHandsTaken(true);
			System.out.println("Team2 has no hand taken");
			return true;
		}
		return false;
	}

}
