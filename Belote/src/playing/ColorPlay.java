package playing;

import cards.*;
import players.Player;
import random.RandomGenerator;
import userinput.UserInput;

import java.util.ArrayList;
import java.util.List;

import announcement.Choice;
import announcement.Contract;

public class ColorPlay extends AllTrumpsPlay {
	private static final int ALL_HANDS_TAKEN=260;
	private static final int COEFFICIENT=1;
	private static final double ROUNDING =0.6;

	private boolean hasTrumped;

	public ColorPlay(List<Player> players, int turn, Choice choice, RandomGenerator rand, UserInput input) {
		super(players, turn, choice, rand, input);
		this.hasTrumped = false;

	}

	public boolean isHasTrumped() {
		return hasTrumped;
	}

	public void setHasTrumped(boolean hasTrumped) {
		this.hasTrumped = hasTrumped;
	}

	@Override
	public int getAllHandsTaken() {
		return ALL_HANDS_TAKEN;
	}

	@Override
	public double getPartFromWhereToRoundUp() {
		return ROUNDING;
	}

	@Override
	public int getCardPoints(Card c) {
		if (hasTrumpSuit(c)) {
			return c.getAllTrumpsPoints();
		}
		return c.getNoTrumpsPoints();
	}

	public boolean hasTrumpSuit(Card card) {
		return card.getSuit().toString().equals(super.getChoice().getContract().getContractName().toUpperCase());
	}

	public boolean isTrumpSuit(Suit suit) {
		return suit.toString().equals(super.getChoice().getContract().getContractName().toUpperCase());
	}

	public Suit getTrumpSuit() {
		return Suit.valueOf(super.getChoice().getContract().getContractName().toUpperCase());
	}

	@Override
	public int determinePlayerWithStrongestCard() {
		if (!hasTrumped) {
			return super.determinePlayerWithStrongestCard();
		}
		return super.personAndCardsOnTableGetPerson(getStrongest(getTrumpSuit()));

	}

	@Override
	public int personChoice(Player player, int cardThrown) {
		int cardIndex = 0;
		if (cardThrown == 1) {
			cardIndex = super.getUserInput().chooseCardFromList(player.getHand(), 1, player.getHand().size() - 1);
			hasTrumped = false;
		} else {

			Card first = getFirstThrownCard();
			Suit suit = first.getSuit();
			List<Integer> choice;
			int strongerCard = 0;
			if (super.containsSuit(suit, player)) {

				cardIndex = personChoiceOfSameSuit(player, suit);

			} else {
				if (containsTrumpSuit(player)) {
					choice = new ArrayList<>();
					cardIndex = personChoiceOfTrumpSuits(player, strongerCard, choice);

				} else {
					cardIndex = super.getUserInput().chooseCardFromList(player.getHand(), 1,
							player.getHand().size() - 1);
				}
			}

		}
		return cardIndex;

	}

	@Override
	public int personChoiceOfSameSuit(Player player, Suit suit) {
		if (isTrumpSuit(suit)) {
			return super.personChoiceOfSameSuit(player, suit);
		} else {
			List<Integer> choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));

			return super.getUserInput().chooseCardFromList(choice, choice.get(0) + 1, choice.get(choice.size() - 1));
		}
	}

	@Override
	public int computerChoiceOfSameSuit(Player player, Suit suit) {
		if (isTrumpSuit(suit)) {
			
			return super.computerChoiceOfSameSuit(player, suit);
		} else {
			List<Integer> choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));

			return super.getRand().generateNumber(choice.get(0), choice.size());

		}
	}

	@Override
	public int computerChoice(Player player, int cardThrown) {
		int cardIndex = 0;
		if (cardThrown == 1) {
			cardIndex = super.getRand().generateNumber(0, player.getHand().size());
			hasTrumped = false;
		} else {
			int strongerCard = 0;
			Card first = getFirstThrownCard();
			Suit suit = first.getSuit();
			List<Integer> choice;
			if (super.containsSuit(suit, player)) {

				cardIndex = computerChoiceOfSameSuit(player, suit);
			} else {
				if (containsTrumpSuit(player)) {
					choice = new ArrayList<>();
					cardIndex = computerChoiceOfTrumpSuits(player, strongerCard, choice);

				} else {
					cardIndex = super.getRand().generateNumber(0, player.getHand().size());
				}
			}

		}
		return cardIndex;

	}

	public int personChoiceOfTrumpSuits(Player player, int strongerCard, List<Integer> choice) {
		if (!hasTrumped) {
			choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().compareTo(getTrumpSuit()) == 0);
			hasTrumped = true;
			return super.getUserInput().chooseCardFromList(choice, choice.get(0) + 1, choice.get(choice.size() - 1));

		} else {
			strongerCard = containsStrongerCardOfSuit(super.getStrongest(getTrumpSuit()), player);
			if (strongerCard != -1) {
			
				choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().compareTo(getTrumpSuit()) == 0
						&& compareTwoCards.compare(x, super.getStrongest(getTrumpSuit())) == 1);
				return super.getUserInput().chooseCardFromList(choice, choice.get(0) + 1,
						choice.get(choice.size() - 1));
				

			} else {
				
				return super.getUserInput().chooseCardFromList(player.getHand(), 1, player.getHand().size() - 1);

			}

		}

	}

	public int computerChoiceOfTrumpSuits(Player player, int strongerCard, List<Integer> choice) {
		if (!hasTrumped) {
			choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().compareTo(getTrumpSuit()) == 0);
			hasTrumped = true;
			return super.getRand().generateNumber(choice.get(0), choice.size());

		} else {
			strongerCard = containsStrongerCardOfSuit(super.getStrongest(getTrumpSuit()), player);
			if (strongerCard != -1) {

				choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().compareTo(getTrumpSuit()) == 0
						&& compareTwoCards.compare(x, super.getStrongest(getTrumpSuit())) == 1);
				return super.getRand().generateNumber(choice.get(0), choice.size());

			} else {
				return super.getRand().generateNumber(0, player.getHand().size());
			}

		}

	}

	public boolean containsTrumpSuit(Player player) {
		List<Card> hand = player.getHand();
		for (int i = 0; i < hand.size(); i++) {
			if (hasTrumpSuit(hand.get(i))) {
				return true;
			}

		}
		return false;
	}

}
