package playing;

import cards.*;
import userinput.*;
import bonuses.*;
import players.Player;
import players.PlayerType;
import random.RandomGenerator;
import view.View;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import announcement.Choice;
import announcement.Contract;

public class AllTrumpsPlay extends NoTrumpsPlay {

	private static final int COEFFICIENT = 1;
	private static final double ROUNDING = 0.4;

	public AllTrumpsPlay(List<Player> players, int turn, Choice choice, RandomGenerator rand, UserInput input) {
		super(players, turn, choice, rand, input);
		for (int i = 0; i < players.size(); i++) {
			players.get(i).setBelote(getBelote(players.get(i).getHand(), super.getChoice().getContract()));

		}

	}

	@Override
	public int getCardPoints(Card c) {
		return c.getRank().getAllTrumpsPoints();
	}

	@Override
	public int getCoefficient() {
		return COEFFICIENT;
	}

	@Override
	public double getPartFromWhereToRoundUp() {
		return ROUNDING;
	}

	public int containsStrongerCardOfSuit(Card card, Player player) {
		List<Card> hand = player.getHand();
		for (int i = 0; i < hand.size(); i++) {
			if (hand.get(i).getSuit() == card.getSuit() && compareTwoCards.compare(hand.get(i), card) == 1) {
				return i;
			}

		}
		return -1;

	}

	@Override
	public Card playATurn(Player player, int cardThrown) {
		int cardIndex = chooseCard(player, cardThrown);
		Card removed = player.getHand().get(cardIndex);
		if (player.getHand().size() == super.getMAX_COUNT_CARDS() && getAllBonuses(player).size() > 0) {
			announceBonuses(player);

		}
		if ((cardThrown == 1 || removed.getSuit().equals(super.getFirstThrownCard().getSuit()))
				&& (player.beloteContains(removed))) {

			announceBelote(player, removed);

		}

		player.getHand().remove(removed);

		return putCardOnTable(player, cardThrown, removed);
	}

	public int personChoiceOfSameSuit(Player player, Suit suit) {
		int strongerCard = containsStrongerCardOfSuit(super.getStrongest(suit), player);
		List<Integer> choice;
		if (strongerCard == -1) {

			choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));

		} else {
			choice = super.getCardsOfCurrentSuit(player.getHand(),
					x -> x.getSuit().equals(suit) && compareTwoCards.compare(x, super.getStrongest(suit)) == 1);
		}

		return super.getUserInput().chooseCardFromList(choice, choice.get(0) + 1, choice.get(choice.size() - 1));

	}

	public int computerChoiceOfSameSuit(Player player, Suit suit) {
		int strongerCard = containsStrongerCardOfSuit(super.getStrongest(suit), player);
		List<Integer> choice;
		if (strongerCard == -1) {
			choice = super.getCardsOfCurrentSuit(player.getHand(), x -> x.getSuit().equals(suit));
		} else {

			choice = super.getCardsOfCurrentSuit(player.getHand(),
					x -> x.getSuit().equals(suit) && compareTwoCards.compare(x, super.getStrongest(suit)) == 1);

		}

		return super.getRand().generateNumber(choice.get(0), choice.size());
	}

	public List<Bonus> announceBonuses(Player player) {
		String answer = "";
		List<Bonus> bonus = getAllBonuses(player);
		if (player.getType() == PlayerType.PERSON) {
			System.out.println("Will you announce your bonuses:");
			for (int i = 0; i < bonus.size(); i++) {
				System.out.println(bonus.get(i).toString());
				answer = personAnnouncement(player);
				if (answer.equals("yes")) {
					player.addBonus(bonus.get(i));
					System.out.println(player.getName() + " announced bonuses!" + player.getBonuses().toString());
					View.slowDown();

				}
			}

		} else {
			for (int i = 0; i < bonus.size(); i++) {
				System.out.println(bonus.get(i).toString());
				answer = computerAnnouncement();
				if (answer.equals("yes")) {
					player.addBonus(bonus.get(i));
					System.out.println(player.getName() + " announced bonuses!" + player.getBonuses().toString());
					View.slowDown();

				}
			}
		}
		return player.getBonuses();

	}

	public boolean announceBelote(Player player, Card card) {
		String answer = "";
		if (player.getType().equals(PlayerType.PERSON)) {
			System.out.println("Will you announce belote? ");
			answer = personAnnouncement(player);

		} else {
			answer = computerAnnouncement();

		}

		if (answer.equals("yes")) {

			removeBeloteCards(player, card);
			player.setBeloteCount(player.getBeloteCount() + 1);
			System.out.println(player.getName() + " " + "announced belote!");

			return true;
		}
		return false;
	}

	public String computerAnnouncement() {
		int number = super.getRand().generateNumber(0, 2);
		return (number == 1) ? "yes" : "no";

	}

	public String personAnnouncement(Player player) {
		String answer = "";
		answer = super.getUserInput().getInput();
		while ((!answer.equals("yes") && !answer.equals("no"))) {
			System.out.println("Enter yes or no");
			answer = super.getUserInput().getInput();

		}
		return answer;
	}

	public List<Card> removeBeloteCards(Player player, Card card) {
		List<Card> belote = player.getBelote().stream().filter(x -> !x.getSuit().equals(card.getSuit()))
				.collect(Collectors.toList());
		player.setBelote(belote);
		return belote;
	}

	public boolean compareBonuses() {
		List<Bonus> team1 = new ArrayList<>();
		List<Bonus> team2 = new ArrayList<>();
		for (int i = 0; i < super.getPlayers().size(); i++) {
			if (super.getPlayers().get(i).getTeam().equals("Team1")) {
				team1.addAll(super.getPlayers().get(i).getBonuses());
			} else {
				team2.addAll(super.getPlayers().get(i).getBonuses());
			}
		}
		if (team1.isEmpty() && team2.isEmpty() || team1.isEmpty() || team2.isEmpty()) {
			return false;
		}
		removeBonuses(team1, team2);
		return true;

	}

	public void removeBonuses(List<Bonus> team1, List<Bonus> team2) {
		String strongerSequence = comparePointsSequenceBonuses(team1, team2);
		String strongerSquare = comparePointsSquareBonuses(team1, team2);
		if (strongerSequence != "") {
			removeBonus(strongerSequence, x -> (x.getType().toString().startsWith("SQAURE")));

		} else {
			removeBonus(x -> (x.getType().toString().startsWith("SQAURE")));

		}
		if (strongerSquare != "") {
			removeBonus(strongerSquare, x -> !x.getType().toString().startsWith("SQAURE"));

		} else {
			removeBonus(x -> !x.getType().toString().startsWith("SQAURE"));
		}

	}

	public void removeBonus(Predicate<Bonus> bonus) {
		for (int i = 0; i < super.getPlayers().size(); i++) {
			List<Bonus> left = super.getPlayers().get(i).getBonuses().stream().filter(bonus)
					.collect(Collectors.toList());
			super.getPlayers().get(i).setBonuses(left);
		}
	}

	public void removeBonus(String team, Predicate<Bonus> bonus) {
		for (int i = 0; i < super.getPlayers().size(); i++) {
			if (!super.getPlayers().get(i).getTeam().equals(team)) {
				List<Bonus> left = super.getPlayers().get(i).getBonuses().stream().filter(bonus)
						.collect(Collectors.toList());
				super.getPlayers().get(i).setBonuses(left);
			}

		}

	}

	public String comparePointsSequenceBonuses(List<Bonus> team1, List<Bonus> team2) {

		int max1 = getMaxLength(team1);
		int max2 = getMaxLength(team2);
		if ((max1 == max2) && max1 > 0 && max2 > 0) {
			int maxPoints1 = team1.stream().filter(x -> x.getBonus().size() == max1)
					.map(x -> x.getSumOfBonusCardsPoints()).reduce((a, b) -> Integer.max(a, b)).get();
			int maxPoints2 = team2.stream().filter(x -> x.getBonus().size() == max2)
					.map(x -> x.getSumOfBonusCardsPoints()).reduce((a, b) -> Integer.max(a, b)).get();
			if (maxPoints1 == maxPoints2) {
				return "";
			} else if (maxPoints1 > maxPoints2) {
				return "Team1";
			} else {
				return "Team2";
			}

		} else if (max1 > max2) {
			return "Team1";

		} else {
			return "Team2";
		}

	}

	public String comparePointsSquareBonuses(List<Bonus> team1, List<Bonus> team2) {
		int maxPointsTeam1 = getMaxNumOfPoints(team1);
		int maxPointsTeam2 = getMaxNumOfPoints(team2);

		if (maxPointsTeam1 > maxPointsTeam2) {
			return "Team1";
		} else if (maxPointsTeam1 < maxPointsTeam2) {
			return "Team2";
		}
		return "";

	}

	public int getMaxLength(List<Bonus> bonuses) {
		if (containsType(bonuses, x -> (!x.startsWith("SQUARE")))) {
			return bonuses.stream().filter(x -> (!(x.getType().toString().startsWith("SQUARE"))))
					.map(x -> x.getBonus().size()).reduce((a, b) -> Integer.max(a, b)).get();
		}
		return 0;
	}

	public int getMaxNumOfPoints(List<Bonus> bonuses) {
		if (containsType(bonuses, x -> x.startsWith("SQUARE"))) {
			return bonuses.stream().filter(x -> x.getType().toString().startsWith("SQUARE"))
					.map(x -> x.getType().getPoints()).reduce((a, b) -> Integer.max(a, b)).get();
		}
		return 0;

	}

	public void addBonusesPoints() {
		for (int i = 0; i < super.getPlayers().size(); i++) {
			super.getPlayers().get(i).addPoints(getPointsOfBonuses(super.getPlayers().get(i).getBonuses()));

		}

	}

	public int getPointsOfBonuses(List<Bonus> bonuses) {
		int sum = 0;
		for (int i = 0; i < bonuses.size(); i++) {
			sum += bonuses.get(i).getPoints();
		}
		return sum;
	}

	public void addBelotePoints() {

		for (int i = 0; i < super.getPlayers().size(); i++) {
			super.getPlayers().get(i).addPoints(super.getPlayers().get(i).getBeloteCount() * 20);
		}

	}

	@Override
	public boolean addFinalRoundPoints() {
		addTeamPoints();
		super.checkTeamForNoHands();
		compareBonuses();
		addBonusesPoints();
		addBelotePoints();
		redistributeTeamPoints();
		if (checkWhoAreWinners().equals("not announced")) {
			movePointsFromOneTeamToOther();
			super.redistributeTeamPoints();
			return true;
		}
		return false;
	}

	public List<Bonus> consecutiveSuits(List<Card> hand) {
		List<Bonus> bonuses = new ArrayList<>();
		Set<Card> bonus = new HashSet<>();

		for (int i = 0; i < hand.size() - 1; i++) {

			if (hand.get(i).getSuit() == hand.get(i + 1).getSuit()
					&& hand.get(i + 1).getRankValue() - hand.get(i).getRankValue() == 1) {
				bonus.add(hand.get(i));
				bonus.add(hand.get(i + 1));

			} else {
				if (bonus.size() >= 3) {
					bonuses.add(new Bonus(bonus, determineTypeSequences(bonus.size())));
					bonus.clear();

				}
				if (!bonus.isEmpty()) {
					bonus = new HashSet<>();
				}
			}

		}
		if (bonus.size() >= 3) {
			if (bonus.size() == 8) {
				separateBonus(bonus, bonuses, 3);

			} else {
				bonuses.add(new Bonus(bonus, determineTypeSequences(bonus.size())));
			}
		}

		return bonuses;

	}

	private void separateBonus(Set<Card> bonus, List<Bonus> bonuses, int n) {
		List<Card> bonusSeparator = new ArrayList<>(bonus);
		Collections.sort(bonusSeparator, Comparator.comparing(Card::getRankValue));
		Set<Card> separatedBonuses = new HashSet<>();
		for (int i = 0; i < n; i++) {
			separatedBonuses.add(bonusSeparator.get(i));
			bonusSeparator.remove(i);

		}
		bonuses.add(new Bonus(separatedBonuses, determineTypeSequences(n)));
		bonuses.add(new Bonus(new HashSet<>(bonusSeparator), determineTypeSequences(bonusSeparator.size())));

	}

	public BonusType determineTypeSequences(int n) {
		if (n == 3) {
			return BonusType.TIERCE;
		} else if (n == 4) {
			return BonusType.QUARTE;
		} else {
			return BonusType.QUINT;

		}

	}

	public List<Bonus> getSquares(List<Card> hand) {
		List<Card> handCopy = new ArrayList<>(hand);
		Collections.sort(handCopy, Comparator.comparing(Card::getRankValue).thenComparing(Card::getSuitValue));
		List<Bonus> bonuses = new ArrayList<>();
		Set<Card> square = new HashSet<>();
		int counter = 0;
		for (int j = 0; j < handCopy.size() - 1; j++) {
			if (handCopy.get(j).getRankValue() > 8 && (handCopy.get(j).getRank() == handCopy.get(j + 1).getRank())) {
				counter++;
				square.add(handCopy.get(j));
				square.add(handCopy.get(j + 1));

			} else {
				square = new HashSet<>();
				counter = 0;
			}
			if (counter == 3) {
				bonuses.add(new Bonus(square, BonusType.getGetBonusByName().get(handCopy.get(j).getRank().toString())));
				square.clear();
				counter = 0;

			}

		}

		return bonuses;
	}

	public List<Card> getBelote(List<Card> hand, Contract contract) {

		List<Card> belote = new ArrayList<>();
		for (int i = 0; i < hand.size() - 1; i++) {
			if (hand.get(i).getRank() == Rank.QUEEN && hand.get(i + 1).getRank() == Rank.KING) {
				belote.add(hand.get(i));
				belote.add(hand.get(i + 1));
			}
		}
		if (contract != Contract.ALL_TRUMPS) {
			List<Card> colorGame = belote.stream()
					.filter(x -> x.getSuit().toString().equals(contract.getContractName().toUpperCase()))
					.collect(Collectors.toList());
			return colorGame;
		}

		return belote;

	}

	public boolean containsType(List<Bonus> bonuses, Predicate<String> predicate) {
		for (int i = 0; i < bonuses.size(); i++) {
			if (predicate.test(bonuses.get(i).getType().toString())) {
				return true;
			}

		}
		return false;

	}

	public List<Bonus> getAllBonuses(Player player) {

		List<Bonus> allBonuses = getSquares(player.getHand());
		List<Bonus> sequence = consecutiveSuits(player.getHand());
		removeОverlapping(allBonuses, sequence);
		allBonuses.addAll(sequence);

		return allBonuses;
	}

	public boolean removeОverlapping(List<Bonus> square, List<Bonus> sequence) {
		if (square.size() == 0 || sequence.size() == 0) {
			return false;
		}
		Bonus seq = sequence.get(0);
		Bonus squ = square.get(0);
		if (squ.contains(seq.getMin()) || squ.contains(seq.getMax())) {
			sequence.remove(0);
			return true;
		}

		return false;

	}

}