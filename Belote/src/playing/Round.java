package playing;

import view.*;
import cards.*;
import dealing.Dealing;

import java.util.List;
import announcement.*;

import players.*;
import random.RandomGenerator;
import userinput.*;

public class Round {
	private Dealing dealing;
	private Announcement announce;
	private int turn;
	private RoundPlay roundPlay;
	private List<Player> players;
	private Choice choice;

	public Round(Game game) {
		this.players = game.getPlayers();
		this.turn = game.getTurn();
	}

	public void setPoints() {

		roundPlay.addFinalRoundPoints();
	}

	public Announcement getAnnounce() {
		return announce;
	}

	public RoundPlay getRoundPlay() {
		return roundPlay;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public RoundPlay chooseTypeOfGame(Choice choice, RandomGenerator rand, UserInput input) {

		switch (choice.getContract()) {

		case ALL_TRUMPS:
			return new AllTrumpsPlay(players, turn, choice, rand, input);

		case SPADES:
			return new ColorPlay(players, turn, choice, rand, input);

		case HEARTS:
			return new ColorPlay(players, turn, choice, rand, input);

		case DIAMONDS:
			return new ColorPlay(players, turn, choice, rand, input);

		case CLUBS:
			return new ColorPlay(players, turn, choice, rand, input);

		default:
			return new NoTrumpsPlay(players, turn, choice, rand, input);

		}

	}

	public boolean areHanging(int team1, int team2) {
		return team1 != team2;

	}

	public int getTurn() {
		return turn;
	}

	public int setTurn(int turn) {
		this.turn = (turn <= 3) ? turn : 0;
		return this.turn;
	}

	public void start() {
		this.dealing = new Dealing(players, turn);
		this.dealing.deal(3);
		this.dealing.deal(2);
		setTurn(turn + 1);
		this.announce = new Announcement(new UserInput());
		this.choice = announce.announce(players,turn);
		this.dealing.deal(3);
		setTurn(turn + 1);
		this.roundPlay = chooseTypeOfGame(choice, new RandomGenerator(System.nanoTime()), new UserInput());
		this.roundPlay.startPlaying();

	}


}
