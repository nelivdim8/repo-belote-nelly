package playing;

import cards.*;
import dealing.Dealing;
import userinput.*;
import random.*;
import players.*;
import view.View;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.Scanner;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import announcement.Choice;
import announcement.Contract;

public abstract class RoundPlay {

	private Map<Integer, Card> personAndCardsOnTable;
	private Map<Integer, Integer> cardNumberAndPerson;
	private final static int MAX_COUNT_CARDS = 8;
	private final Choice choice;
	private int roundPointsTeam1;
	private int roundPointsTeam2;
	private List<Player> players;
	private int turn;
	private RandomGenerator rand;
	private UserInput userInput;
	private int hangingPoints;
	private boolean areAllHandsTaken;

	public RoundPlay(List<Player> players, int turn, Choice choice, RandomGenerator rand, UserInput input) {
		this.players = players;
		this.personAndCardsOnTable = new HashMap<>();
		this.cardNumberAndPerson = new HashMap<>();
		this.choice = choice;
		this.turn = turn;
		this.rand = rand;
		this.userInput = input;
		this.areAllHandsTaken = false;

	}

	public RandomGenerator getRand() {
		return rand;
	}

	public void setRand(RandomGenerator rand) {
		this.rand = rand;
	}

	public void setTurn(int turn) {
		this.turn = (turn <= 3) ? turn : 0;

	}

	public Map<Integer, Card> getCardsOnTable() {
		return personAndCardsOnTable;
	}

	public static int getMAX_COUNT_CARDS() {
		return MAX_COUNT_CARDS;
	}

	public Map<Integer, Integer> getCardNumberAndPerson() {
		return cardNumberAndPerson;
	}

	public Choice getChoice() {
		return choice;
	}

	public int getRoundPointsTeam1() {
		return roundPointsTeam1;
	}

	public int getRoundPointsTeam2() {
		return roundPointsTeam2;
	}

	public UserInput getUserInput() {
		return userInput;
	}

	public List<Player> getPlayers() {
		return players;
	}

	public int getTurn() {
		return turn;
	}

	public void setRoundPointsTeam1(int roundPointsTeam1) {
		this.roundPointsTeam1 = roundPointsTeam1;
	}

	public void setRoundPointsTeam2(int roundPointsTeam2) {
		this.roundPointsTeam2 = roundPointsTeam2;
	}

	public abstract int chooseCard(Player player, int cardThrown);

	public abstract Card playATurn(Player player, int cardThrown);

	public abstract Card putCardOnTable(Player player, int cardThrown, Card removed);

	public abstract int getCardPoints(Card c);

	public abstract int determinePlayerWithStrongestCard();

	public abstract int scoring(String team);

	public abstract int personChoice(Player player, int cardThrown);

	public abstract int computerChoice(Player player, int cardThrown);

	public abstract int declareHandTaker();

	public abstract int sumAllPoints();

	public abstract boolean addFinalRoundPoints();

	public abstract String checkWhoAreWinners();

	public abstract void movePointsFromOneTeamToOther();

	public boolean areAllHandsTaken() {
		return areAllHandsTaken;
	}

	public void setAreAllHandsTaken(boolean areAllHandsTaken) {
		this.areAllHandsTaken = areAllHandsTaken;
	}

	public int startPlaying() {
		int count = 0;
		while (!areEmptyHands()) {

			playATurn(players.get(turn), count + 1);

			Dealing.clear();
			System.out.println("Type of game: " + choice);
			System.out.println("Announced team: "+choice.getAnnouncedTeam());
			printCardsOnTable();
			System.out.println(players.get(0).handToString());
			View.slowDown();

			count++;
			if (count == players.size()) {

				setTurn(declareHandTaker());
				if (areEmptyHands()) {
					players.get(turn).addPoints(10);

				}
				View.slowDown();

				count = 0;
			} else {
				setTurn(turn + 1);
			}

		}

		return turn + 1;

	}

	public boolean areEmptyHands() {
		for (int i = 0; i < players.size(); i++) {
			if (!players.get(i).getHand().isEmpty()) {
				return false;
			}

		}
		return true;
	}

	public void printCardsOnTable() {
		personAndCardsOnTable.entrySet().stream().forEach(x -> System.out.println(players.get(x.getKey()).getName()
				+ "(" + players.get(x.getKey()).getTeam() + ")" + " :" + x.getValue()));
	}

	public int getHangingPoints() {
		return hangingPoints;
	}

	public String getRoundWinners() {
		if (this.roundPointsTeam1 > this.roundPointsTeam2) {
			return "Team1";
		}
		if (this.roundPointsTeam1 < this.roundPointsTeam2) {
			return "Team2";
		}
		return "";

	}

	public Card getFirstThrownCard() {
		int personNumber = cardNumberAndPerson.get(1);
		Card first = getCardsOnTable().get(personNumber);
		return first;
	}



	public void addHangingPoints(int hangingPoints) {
		if (this.hangingPoints != 0) {
			this.hangingPoints += hangingPoints;
			if (!areAllHandsTaken) {
				if (choice.isContra()) {
					hangingPoints *= 2;
				}
				if (choice.isRecontra()) {
					hangingPoints *= 2;

				}
			}
		} else {
			this.hangingPoints = hangingPoints;

		}

	}

	




public void addTeamPoints(){

	for (int i = 0; i < players.size(); i++) {
		if (players.get(i).getTeam().equals("Team1")) {
			this.roundPointsTeam1 += players.get(i).getPoints();
		} else {
			this.roundPointsTeam2 += players.get(i).getPoints();
		}

	}
  

}



public void redistributeTeamPoints() {
	this.roundPointsTeam1 = 0;
	this.roundPointsTeam2 = 0;

	addTeamPoints();

}


public boolean containsSuit(Suit suit, Player player) {
	List<Card> hand = player.getHand();
	for (int i = 0; i < hand.size(); i++) {
		if (hand.get(i).getSuit() == suit) {
			return true;
		}

	}
	return false;
}

public List<Integer> getCardsOfCurrentSuit(List<Card> hand, Predicate<Card> predicate) {
	List<Integer> accessibleCards = new ArrayList<>();
	for (int i = 0; i < hand.size(); i++) {
		if (predicate.test(hand.get(i))) {
			accessibleCards.add(i);
		}

	}
	return accessibleCards;

}

public int personAndCardsOnTableGetPerson(Card value) {
	for (Object o : personAndCardsOnTable.keySet()) {
		if (personAndCardsOnTable.get(o).equals(value)) {
			return (Integer) o;
		}
	}
	return -1;
}


}
