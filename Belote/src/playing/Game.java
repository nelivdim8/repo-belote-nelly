package playing;

import players.*;
import view.View;

import java.util.ArrayList;

import java.util.List;
import java.util.Scanner;

public class Game {

	private final static String TEAM1 = "Team1";
	private final static String TEAM2 = "Team2";
	private int team1Points;
	private int team2Points;
	private List<Player> players;
	private Round round;
	private int turn;
	private int hangingPoints;
	private String announced;
	private Scanner sc = new Scanner(System.in);


	public void startGame() {

		this.turn = 0;
		System.out.println("Enter your name:");
		String name = sc.next();
		players = putPlayersInTeams(name);

		while (getTeam1Points() < 151 && getTeam2Points() < 151) {
			this.round = new Round(this);
			round.start();
			round.setPoints();
			setTurn(round.setTurn(turn+1));
			addTeamPoints(round.getRoundPlay().scoring(TEAM1), round.getRoundPlay().scoring(TEAM2),
					round.getRoundPlay().getChoice().getAnnouncedTeam(), round.getRoundPlay().getRoundWinners());
			setPlayersAttributes();
			System.out.println("Points team1: " + team1Points);
			System.out.println("Points team2: " + team2Points);
			View.slowDown();
			
		

		}
		System.out.println("Do you want to play again?");
		String answer = sc.next();
		if (answer.equals("yes")) {
			setTeam1Points(0);
			setTeam2Points(0);
			startGame();
		}
	}

	public List<Player> putPlayersInTeams(String name) {
		players = new ArrayList<>();
		players.add(new Player(name, PlayerType.PERSON, "Team1"));
		players.add(new Player("Player2", PlayerType.COMPUTER, "Team2"));
		players.add(new Player("Player3", PlayerType.COMPUTER, "Team1"));
		players.add(new Player("Player4", PlayerType.COMPUTER, "Team2"));
		return players;
	}

	public int getTeam1Points() {
		return team1Points;
	}

	public void setTeam1Points(int team1Points) {
		this.team1Points = team1Points;
	}

	public int getTeam2Points() {
		return team2Points;
	}

	public void setTeam2Points(int team2Points) {
		this.team2Points = team2Points;
	}

	public void addTeam1Points(int team1Points) {
		this.team1Points += team1Points;
	}

	public void addTeam2Points(int team2Points) {
		this.team2Points += team2Points;
	}

	public void addTeamPoints(int team1Points, int team2Points, String announced, String winners) {
		String notAnnounced = getOppositeTeam(announced);
		String loserTeam = getOppositeTeam(winners);
		
		if (winners.equals("")) {
			
			if (hangingPoints != 0) {
				addTeamPoints(team1Points + hangingPoints, notAnnounced);

			} else {
				
				setHangingPoints(this.round.getRoundPlay().getHangingPoints());
				addTeamPoints(team1Points, notAnnounced);
			}
		} else {
			if (hangingPoints != 0) {
				addTeamPoints(Math.max(team1Points, team2Points) + hangingPoints, winners);
				addTeamPoints(Math.min(team1Points, team2Points), loserTeam);
				setHangingPoints(0);

			} else {
				System.out.println("Team1: " + team1Points + "\n" + "Team2: " + team2Points);
				this.team1Points += team1Points;
				this.team2Points += team2Points;
			}
		}
	}

	public void addTeamPoints(int points, String other) {
		if (other.equals(TEAM1)) {
			addTeam1Points(points);
		} else {
			addTeam2Points(points);
		}

	}

	public List<Player> getPlayers() {
		return players;
	}

	public int getTurn() {
		return turn;
	}

	public void setTurn(int turn) {
		if (turn > 3) {
			this.turn = 0;
		} else {
			this.turn = turn;
		}
	}

	public static String getTeam1() {
		return TEAM1;
	}

	public static String getTeam2() {
		return TEAM2;
	}

	public void showPoints() {
		System.out.println(TEAM1 + " points: " + team1Points);
		System.out.println(TEAM2 + " points: " + team2Points);
		View.slowDown();
	}

	public void setPlayersAttributes() {
		for (int i = 0; i < players.size(); i++) {
			players.get(i).setBelote(new ArrayList<>());
			players.get(i).setBonuses(new ArrayList<>());
			players.get(i).setHasAnnounced(false);
			players.get(i).setBeloteCount(0);
			players.get(i).setPoints(0);

		}
	}

	public int getHangingPoints() {
		return hangingPoints;
	}

	public void setHangingPoints(int hangingPoints) {
		System.out.println("Hanging points: " + hangingPoints);
		this.hangingPoints = hangingPoints;
	

	}

	public String getOppositeTeam(String team) {
		if (team.equals(TEAM1)) {
			return TEAM2;
		}
		if (team.equals(TEAM2)) {
			return TEAM1;

		}
		return "";
	}
	

}
