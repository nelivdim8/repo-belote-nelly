package userinput;


import java.util.List;
import java.util.Scanner;

import announcement.Choice;
import announcement.Contract;

public class UserInput {
	Scanner input;

	public UserInput() {
	this.input=new Scanner(System.in);
	}
	public String getInput() {
	return input.next();
	}
	public int chooseCardFromList(List<?> hand,int start,int end) {
		System.out.println("Choose a card");
		System.out.print("Enter which card do you want to throw: ");
		boolean oneElement = (hand.size()>1) ? false : true;
		System.out.println((!oneElement) ? " from "+(start)+" to " + (end+1) : (end+1));
		String choice = input.next();
		int intChoice = Integer.parseInt(choice);
		while ((oneElement && intChoice != start) || (!oneElement && (intChoice < start || intChoice > end+1))) {
			System.out.println("Please enter one of the given numbers!");
			choice = input.next();
			intChoice = Integer.parseInt(choice);

		}

		return intChoice - 1;

	}
	public int chooseTypeOfGame(int from, int to) {
	   
		System.out.println("Choose type of game");
		Contract.showOptions(from, to);
		String choice = input.next();
		int intChoice = Integer.parseInt(choice);
		
		while(intChoice<from||intChoice>to) {
			System.out.println("There is no such option!");
			choice = input.next();
			intChoice = Integer.parseInt(choice);
		
	}
		return intChoice;
	}
	public int chooseBetween(int first,int second,Choice max) {
		System.out.println("Choose type of game");
		Contract.showOptions(max.getNumber()+1, second, first);
		String choice = input.next();
		int intChoice = Integer.parseInt(choice);
		
		
		while(intChoice<max.getNumber()+1||(intChoice>second&&intChoice!=first)) {
			System.out.println("There is no such option!");
			choice = input.next();
			intChoice = Integer.parseInt(choice);
		
	}
		if(intChoice!=first) {
			max.setNumber(max.getNumber()+1);
		}
		return intChoice;
		
			
				
		}
		
	
	

}
