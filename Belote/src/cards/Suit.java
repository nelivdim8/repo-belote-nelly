package cards;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import bonuses.BonusType;

public enum Suit {
CLUBS(1,'\u2663'),DIAMONDS(2,'\u2666'),HEARTS(3,'\u2764'),SPADES(4,'\u2660');
private static List<Suit> suits=Arrays.asList(Suit.values());
private int number;
private char symbol;
private static Map<Integer,Suit> getSuitByNumber = new HashMap<>();

static {
	for (Suit suit : Suit.values()) {
		getSuitByNumber.put(suit.number,suit);
	}
}


private Suit(int number,char symbol) {
	this.number = number;
	this.symbol=symbol;
			
}


public int getNumber() {
	return number;
}



public static  List<Suit> getSuits() {
	return suits;
}


public static Map<Integer, Suit> getGetSuitByNumber() {
	return getSuitByNumber;
}


public char getSymbol() {
	return symbol;
}





}
