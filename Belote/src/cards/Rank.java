package cards;

import java.util.Arrays;
import java.util.List;

public enum Rank {
	SEVEN(7, 0, 0,"7"), EIGHT(8, 0, 0,"8"), NINE(9, 14, 0,"9"), TEN(10, 10, 10,"10"), JACK(11, 20, 2,"J"),
	QUEEN(12, 3, 3,"Q"), KING(13, 4,
			4,"K"), ACE(14, 11, 11,"A");
	private static List<Rank> ranks = Arrays.asList(Rank.values());
	private int number;
	private int allTrumpPoints;
	private int noTrumpsPoints;
	private String symbol;

	private Rank(int number, int allTrumpPoints, int noTrumpsPoints,String symbol) {
		this.number = number;
		this.allTrumpPoints = allTrumpPoints;
		this.noTrumpsPoints = noTrumpsPoints;
		this.symbol=symbol;
		
	}

	public int getAllTrumpsPoints() {
		return allTrumpPoints;
	}

	public int getNoTrumpsPoints() {
		return noTrumpsPoints;
	}

	public int getNumber() {
		return number;
	}

	public static List<Rank> getRanks() {
		return ranks;
	}

	public String getSymbol() {
		return symbol;
	}


	

}
