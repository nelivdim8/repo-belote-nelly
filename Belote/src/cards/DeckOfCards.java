package cards;

import java.util.Collections;
import java.util.Stack;

public class DeckOfCards {
	private Stack<Card> cards;
	private int size;

	public DeckOfCards() {

		this.cards = new Stack<>();
		fillDeck();
		Collections.shuffle(cards);

	}

	private void fillDeck() {

		for (int i = 0; i < Suit.getSuits().size(); i++) {
			for (int j = 0; j < Rank.getRanks().size(); j++) {
				this.cards.push(new Card(Rank.getRanks().get(j), Suit.getSuits().get(i)));

			}

		}
	}

	public Stack<Card> getCards() {
		return cards;
	}

	public int getSize() {
		return cards.size();
	}

	@Override
	public String toString() {
		int i = 1;
		String result = "";
		for (Card card : cards) {
			result += (i++) + "." + card+" ";
		}
		return result;
	}

}
