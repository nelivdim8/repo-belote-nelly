
package players;

import cards.*;
import bonuses.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import announcement.*;

public class Player {
	private String name;
	private int points;
	private List<Card> hand;
	private List<Bonus> bonuses;
	private boolean hasAnnounced;
	private final PlayerType type;
	private final String team;
	private int beloteCount;
	private List<Card> belote;
	private static int counter;
	private int index;
	

	public Player(String name, PlayerType type,String team) {

		this.name = name;
		this.hand = new ArrayList<>();
		this.type = type;
		this.team=team;
		this.bonuses=new ArrayList<>();
		this.index=counter++;
	}

	public String getName() {
		return name;
	}

	public int getPoints() {
		return points;
	}

	public void setPoints(int points) {
		this.points = points;
	}

	public List<Card> getHand() {
		return hand;
	}

	public List<Bonus> getBonuses() {
		return bonuses;
	}

	public void setBonuses(List<Bonus> bonuses) {
		this.bonuses = bonuses;
	}
	public void addBonus(List<Bonus> bonuses) {
		this.bonuses.addAll(bonuses);
	}
	public void addBonus(Bonus bonuses) {
		this.bonuses.add(bonuses);
	}


	public boolean getHasAnnounced() {
		return hasAnnounced;
	}

	public void setHasAnnounced(boolean flag) {
		this.hasAnnounced = flag;
	}

	public PlayerType getType() {
		return type;
	}

	public String getTeam() {
		return team;
	}

	public int getBeloteCount() {
		return beloteCount;
	}

	public void setBeloteCount(int count) {
		this.beloteCount=count;
	}
	public void addPoints(int points) {
		this.points+=points;
	}

	public List<Card> getBelote() {
		return belote;
	}

	public void setBelote(List<Card> belote) {
		this.belote = belote;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}
	public String handToString() {
		String result="";
		int i=1;
		for (Card card : hand) {
	    result += (i++) + "." + card+" ";		
	    }
		return result;
	}
	public boolean beloteContains(Card card) {
	return	belote.stream().anyMatch(
				x -> (x.getSuit().equals(card.getSuit()) && x.getRank().equals(card.getRank())));
		
	}
	
	
	
	

}
