package bonuses;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

public enum BonusType {

   TIERCE(20, "Tierce"), QUARTE(50, "Quarte"), QUINT(100, "Quint"), SQUARE_ACES(100, "ACE"), SQUARE_QUEENS(100,
			"QUEEN"), SQUARE_KINGS(100, "KING"), SQUARE_NINES(150, "NINE"), SQUARE_JACKS(200, "JACK");

	private int points;
	private String name;
	private static Map<String, BonusType> getBonusByName = new HashMap<>();

	static {
		for (BonusType type : BonusType.values()) {
			getBonusByName.put(type.name, type);
		}
	}

	private BonusType(int points, String name) {
		this.points = points;
		this.name = name;
	}

	public int getPoints() {
		return points;
	}

	public String getName() {
		return name;
	}

	public static Map<String, BonusType> getGetBonusByName() {
		return getBonusByName;
	}

}
