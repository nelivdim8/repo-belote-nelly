package bonuses;

import cards.*;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Bonus {

	private BonusType type;
	private Set<Card> bonus;

	public Bonus(Set<Card> bonuse, BonusType type) {
		this.bonus =new HashSet<>(bonuse);
		this.type = type;
	}

	@Override
	public String toString() {
		return type.toString();
	}

	public Set<Card> getBonus() {
		return bonus;
	}

	public BonusType getType() {
		return type;
	}

	public int getSumOfBonusCardsPoints() {
		int sum = 0;
		for (Card c : bonus) {
			sum += c.getRankValue();

		}
		return sum;
	}

	public int getPoints() {
		return type.getPoints();
	}

	public List<Card> toList() {
		return new ArrayList<>(bonus);
	}

	public Card getMin() {
		return bonus.stream().min(Comparator.comparing(Card::getRankValue)).get();
	}

	public Card getMax() {
		return bonus.stream().max(Comparator.comparing(Card::getRankValue)).get();
	}

	public boolean contains(Card card) {
		return bonus.stream().anyMatch(x -> x.getRank().equals(card.getRank()) && x.getSuit().equals(card.getSuit()));
	}
	public boolean containsSuit(Suit suit) {
		return bonus.stream().anyMatch(x ->  x.getSuit().equals(suit));
	}
	

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Bonus other = (Bonus) obj;
		if (bonus == null) {
			if (other.bonus != null)
				return false;
		} else if (bonus.size() != other.bonus.size())
			return false;
	
		if (type != other.type)
			return false;
		

		return true;
	}

}
