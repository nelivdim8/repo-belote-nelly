package dealing;
import cards.*;
import playing.*;
import view.View;
import players.*;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class Dealing {
	private static List<Player> players;
	private DeckOfCards deck;
	private int turn;

	public Dealing(List<Player> players, int turn) {
		Dealing.players =players;
		this.turn=turn;
		this.deck = new DeckOfCards();

	
	
	}

	private void dealHelper(int number, int turn) {
		int j = turn;
		for (int i = 0; i < players.size(); i++) {

			players.get(j++).getHand().addAll(getAndRemove(number, players, deck));
			if (j > players.size() - 1) {
				j = 0;
			}
			orderCards(j);
			if(players.get(j).getType()==PlayerType.PERSON) {
				showCards(j);
				View.slowDown();
			}
			


		}

	}

	private List<Card> getAndRemove(int n, List<Player> list, DeckOfCards deck) {
		List<Card> result = new ArrayList<>();

		if (n > 0 && n <= deck.getSize()) {

			for (int i = 0; i < n; i++) {
				result.add(deck.getCards().pop());

			}

		}
		return result;
	}

	public int deal(int number) {

		dealHelper(number, turn);
		return deck.getSize();

	}

	public void orderCards(int playerIndex) {
	
			List<Card> cardsInHand=players.get(playerIndex).getHand();
			Collections.sort(cardsInHand, Comparator.comparing(Card::getSuitValue).thenComparing(Card::getRankValue));
	}

	public void showCards(int personIndex) {
		clear();
        System.out.println(players.get(personIndex).getName() + " "+players.get(personIndex).handToString());

	}

	public void showDeck() {
		System.out.println("Deck");

		System.out.println(deck.toString());

	}

	public static void clear() {
		for (int i = 0; i < 30; i++) {
			System.out.println();
		}
	}

	

}
